# frozen_string_literal: true

module Decidim
  # A form object used to handle user registrations
  class RegistrationForm < Form
    include JsonbAttributes

    mimic :user

    attribute :name, String
    attribute :nickname, String
    attribute :email, String
    attribute :password, String
    attribute :password_confirmation, String
    attribute :newsletter, Boolean
    attribute :tos_agreement, Boolean
    attribute :current_locale, String
    jsonb_attribute :registration_metadata, [
        [:country, String],
        [:privacy_consent, Boolean]
    ]

    validates :name, presence: true
    validates :nickname, presence: true, format: /\A[\w\-]+\z/, length: { maximum: Decidim::User.nickname_max_length }
    validates :email, presence: true, 'valid_email_2/email': { disposable: true }
    validates :password, confirmation: true
    validates :password, password: { name: :name, email: :email, username: :nickname }
    validates :password_confirmation, presence: true
    validates :tos_agreement, allow_nil: false, acceptance: true
    validates :privacy_consent, allow_nil: false, acceptance: true
    validates :country, presence: true

    validate :email_unique_in_organization
    validate :nickname_unique_in_organization
    validate :no_pending_invitations_exist

    def newsletter_at
      return nil unless newsletter?

      Time.current
    end

    def country_for_metadata
      [
          [I18n.t("bulgaria", scope: "decidim.devise.registrations.new.registration_metadata"), "Bulgaria"],
          [I18n.t("belgium", scope: "decidim.devise.registrations.new.registration_metadata"), "Belgium"],
          [I18n.t("bosnia_herzegovina", scope: "decidim.devise.registrations.new.registration_metadata"), "Bosnia & Herzegovina"],
          [I18n.t("czechia", scope: "decidim.devise.registrations.new.registration_metadata"), "Czechia"],
          [I18n.t("montenegro", scope: "decidim.devise.registrations.new.registration_metadata"), "Montenegro"],
          [I18n.t("cyprus", scope: "decidim.devise.registrations.new.registration_metadata"), "Cyprus"],
          [I18n.t("denmark", scope: "decidim.devise.registrations.new.registration_metadata"), "Denmark"],
          [I18n.t("germany", scope: "decidim.devise.registrations.new.registration_metadata"), "Germany"],
          [I18n.t("estonia", scope: "decidim.devise.registrations.new.registration_metadata"), "Estonia"],
          [I18n.t("greece", scope: "decidim.devise.registrations.new.registration_metadata"), "Greece"],
          [I18n.t("spain", scope: "decidim.devise.registrations.new.registration_metadata"), "Spain"],
          [I18n.t("france", scope: "decidim.devise.registrations.new.registration_metadata"), "France"],
          [I18n.t("croatia", scope: "decidim.devise.registrations.new.registration_metadata"), "Croatia"],
          [I18n.t("ireland", scope: "decidim.devise.registrations.new.registration_metadata"), "Ireland"],
          [I18n.t("italy", scope: "decidim.devise.registrations.new.registration_metadata"), "Italia"],
          [I18n.t("kosovo", scope: "decidim.devise.registrations.new.registration_metadata"), "Kosovo"],
          [I18n.t("latvia", scope: "decidim.devise.registrations.new.registration_metadata"), "Latvia"],
          [I18n.t("luxembourg", scope: "decidim.devise.registrations.new.registration_metadata"), "Luxembourg"],
          [I18n.t("lithuania", scope: "decidim.devise.registrations.new.registration_metadata"), "Lithuania"],
          [I18n.t("republic_of_north_macedonia", scope: "decidim.devise.registrations.new.registration_metadata"), "Republic of North Macedonia"],
          [I18n.t("hungary", scope: "decidim.devise.registrations.new.registration_metadata"), "Hungary"],
          [I18n.t("malta", scope: "decidim.devise.registrations.new.registration_metadata"), "Malta"],
          [I18n.t("netherlands", scope: "decidim.devise.registrations.new.registration_metadata"), "Netherlands"],
          [I18n.t("austria", scope: "decidim.devise.registrations.new.registration_metadata"), "Austria"],
          [I18n.t("poland", scope: "decidim.devise.registrations.new.registration_metadata"), "Poland"],
          [I18n.t("portugal", scope: "decidim.devise.registrations.new.registration_metadata"), "Portugal"],
          [I18n.t("romania", scope: "decidim.devise.registrations.new.registration_metadata"), "Romania"],
          [I18n.t("albania", scope: "decidim.devise.registrations.new.registration_metadata"), "Albania"],
          [I18n.t("slovenia", scope: "decidim.devise.registrations.new.registration_metadata"), "Slovenia"],
          [I18n.t("slovakia", scope: "decidim.devise.registrations.new.registration_metadata"), "Slovakia"],
          [I18n.t("serbia", scope: "decidim.devise.registrations.new.registration_metadata"), "Serbia"],
          [I18n.t("finland", scope: "decidim.devise.registrations.new.registration_metadata"), "Finland"],
          [I18n.t("sweden", scope: "decidim.devise.registrations.new.registration_metadata"), "Sweden"],
          [I18n.t("other", scope: "decidim.devise.registrations.new.registration_metadata"), "Other"]
      ]
    end

    private

    def email_unique_in_organization
      errors.add :email, :taken if User.no_active_invitation.find_by(email: email, organization: current_organization).present?
    end

    def nickname_unique_in_organization
      errors.add :nickname, :taken if User.no_active_invitation.find_by(nickname: nickname, organization: current_organization).present?
    end

    def no_pending_invitations_exist
      errors.add :base, I18n.t("devise.failure.invited") if User.has_pending_invitations?(current_organization.id, email)
    end
  end
end
